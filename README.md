# Tiny Micro

This is just a little microservice that just buzzes around
and returns the count of how many characters are in your
request data.

It based off of this [GoKit Example](https://gokit.io/examples/stringsvc.html)
and will eventually grow to have it's own usecase.

## Usage

1. Run the application
```
go run main.go
```
2. Open a separate terminal
3. Send a request to the application
```
curl -X POST -d '{"s":"yeahhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"}' 0.0.0.0:8080/count
```
